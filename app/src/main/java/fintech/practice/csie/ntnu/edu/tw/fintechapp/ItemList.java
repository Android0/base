package fintech.practice.csie.ntnu.edu.tw.fintechapp;

import android.content.Intent;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
//import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemList extends AppCompatActivity {

    @BindView(R.id.textView9)
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        ButterKnife.bind(this);

        Intent intent = this.getIntent();
        email.setText("歡迎，"+intent.getStringExtra("email"));
    }



    @OnClick(R.id.imageView3)
    public void backtohome()
    {
        //Toast.makeText(ItemList.this, "back", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }



    @OnClick({R.id.button5, R.id.button6, R.id.button7, R.id.button8, R.id.button9, R.id.button10, R.id.button11, R.id.button12, R.id.button13, R.id.button14, R.id.button15, R.id.button16})
    public void gotoshowitem(View view)
    {
        //startActivity(new Intent(ItemList.this, ShowItem.class));//no passing value
        //pass button text to next activity
        Intent intent = new Intent(ItemList.this, ShowItem.class);
        Button b = (Button)view;
        String str = b.getText().toString() ;
        intent.putExtra("str",str);
        startActivity(intent);
        //Toast.makeText(ItemList.this, str, Toast.LENGTH_SHORT).show();// output test
    }


}
