package fintech.practice.csie.ntnu.edu.tw.fintechapp;

//import com.appacitive.core.AppacitiveObject;
//import com.appacitive.core.model.Callback;
//import com.coinbase.android.sdk.OAuth;
//import com.coinbase.api2.Coinbase;
//import com.coinbase.api2.entity.OAuthTokensResponse;
//import com.coinbase.api2.exception.UnauthorizedException;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

//import java.util.ArrayList;
//import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowItem extends AppCompatActivity {

    @BindView(R.id.tvItemPrice)
    TextView ItemPrice;
    @BindView(R.id.tvItemName)
    TextView ItemName;
    @BindView(R.id.tvDescribe)
    TextView ItemDescribe;
    @BindView(R.id.imageView4)
    ImageView ItemImage;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_item);
        ButterKnife.bind(this);

        Intent intent = this.getIntent();
        double rate = 23782.96;
        ItemName.setText(intent.getStringExtra("str"));//set default name
        double price=0/rate;
        switch (intent.getStringExtra("str")) {
            case "CPU":
                ItemImage.setImageResource(R.drawable.a1);
                ItemName.setText("Intel i5-6600");//set default name
                price=6900/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("˙第六代 Intel 處理器˙ \n" +
                        "◆ 腳位：1151 \n" +
                        "◆ 時脈速度：3.50-3.90 GHz \n" +
                        "◆ 快取記憶體：6.0 MB \n" +
                        "◆ 核心/執行緒：4 / 4 \n" +
                        "◆ 內含顯示：HD 530 \n" +
                        "◆ TDP：95 W ");//set default describe
                break;
            case "主機板":
                ItemImage.setImageResource(R.drawable.a2);
                ItemName.setText("MSI H170A PC MATE");//set default name
                price=3590/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("★ 支援Socket 1151架構的第六代IntelR酷睿™/奔騰R/賽揚R處理器 \n" +
                        "★ 支援 DDR4-3600 (OC) \n" +
                        "★ DDR4加速引擎(DDR4 Boost)：優化 DDR4 傳輸性能 \n" +
                        "★ USB 3.1 Gen2 速度2倍快：USB 3.1 Gen2 提供比USB 3.1 Gen 1快兩倍的傳輸速度 \n" +
                        "★ 雙重M.2介面(64Gb/s) + Turbo U.2 + USB 3.1 Gen2 Type-A + SATA 6Gb/s \n" +
                        "★ VGA 裝甲設計與多顯卡運算技術：PCI-E 鋼鐵裝甲強化插槽，並支援AMD Crossfire™ \n" +
                        "★ Click BIOS：專為最新系统優化的UEFI BIOS \n" +
                        "★ 第四代軍規料件 (Military Class 4) ：超高穩定料件標準 \n" +
                        "★ 全能防護機制(Guard-Pro) ：最佳的系统防護與省電設置 \n" +
                        "★ 簡易偵錯LED燈(EZ Debug LED) ：最簡單的偵錯設計 \n" +
                        "★ 電流防護設計：防止未預期的突波損害 \n" +
                        "★ 4K高清顯示輸出：超高視覺體驗 \n" +
                        "★ 支援IntelR SBB：提高生產力與資料安全 \n" +
                        "★ 支援Windows 10：100%的兼容性，最佳的性能與系統認證");//set default describe
                break;
            case "RAM":
                ItemImage.setImageResource(R.drawable.a3);
                ItemName.setText("HyperX Fury 8G");//set default name
                price=1299/rate;

                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("自動超頻至高達 1866MHz \n" +
                        "採用非對稱散熱片，既能散熱又具時尚感 \n" +
                        "與 Intel 及 AMD 平台相容 \n" +
                        "提供隨插即用的功能 \n" +
                        "100% 原廠產品測試 \n" +
                        "原廠終身保固");//set default describe
                break;
            case "傳統硬碟":
                ItemImage.setImageResource(R.drawable.a4);
                ItemName.setText("來路不明的硬碟");//set default name
                price=2000/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("■256位元AES資料加密 \n" +
                        "■專屬資料管理軟體 \n" +
                        "■原廠三年保固 ");//set default describe
                break;
            case "固態硬碟":
                ItemImage.setImageResource(R.drawable.a5);
                ItemName.setText("Intel 540S 240G");//set default name
                price=2699/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("效能、品質、價值的完美結合 \n" +
                        "最新 16nm 奈米製程 \n" +
                        "採用 SATA 6Gb/s 介面 \n" +
                        "最高達 560MB/s 的讀取速度 \n" +
                        "最高達 480MB/s 的寫入速度 \n" +
                        "隨機 4K 讀取：74,000 IOPS \n" +
                        "隨機 4K 寫入：85,000 IOPS \n" +
                        "原廠五年有限保固");//set default describe
                break;
            case "Power":
                ItemImage.setImageResource(R.drawable.a6);
                ItemName.setText("海盜船CX750");//set default name
                price=5000/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("◆前置雙12公分的白光LED風扇 \n" +
                        "◆後置12公分風扇增加對流幫助散熱 \n" +
                        "◆8個背板擴充槽 \n" +
                        "◆兩年機殼內部零件免費保固");//set default describe
                break;
            case "機殼":
                ItemImage.setImageResource(R.drawable.a7);
                ItemName.setText("Thermaltake level 10");//set default name
                price=10000/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("全球僅限量300台的《Level 10極光銀限量版機殼》，\n" +
                                    "揉合現代化和未來主義設計，集前衛和優雅於一身；\n"+
                        "以簡約大方的內斂外型展現其卓越性能。\n"+
                        "打破傳統制式結構的創新設計，採用全鋁製機身，\n"+
                        "搭配細緻的噴砂工藝，呈現高質感金屬銀色外觀，\n"+
                        "更結合紅色LED燈光，呈現出最細膩的優質品味。");//set default describe
                break;
            case "顯示卡":
                ItemImage.setImageResource(R.drawable.a8);
                ItemName.setText("Nvidia GTX1080");//set default name
                price=26900/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("◆顯示晶片：NVIDIA GeForce® GTX 1080 \n" +
                        "◆記憶體：8GB GDDR5X \n" +
                        "◆晶片核心時脈：1847/1708 MHz (OC mode) \n" +
                        "◆記憶體時脈：10108 MHz \n" +
                        "◆記憶體介面：256 bit \n" +
                        "◆介面規格：PCI-E x16 3.0 \n" +
                        "◆輸出端子：DP 1.4x3 / HDMI 2.0 / DL-DVI-D");//set default describe
                break;
            case "滑鼠":
                ItemImage.setImageResource(R.drawable.a9);
                ItemName.setText("羅技滑鼠");//set default name
                price=1500/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("◆ 左右手通用設計 \n" +
                        "◆ 獨家時脈條效技術 \n" +
                        "◆ 機械式樞軸按鈕設計 \n" +
                        "◆ 11個可自訂按鈕 \n" +
                        "◆ 1680萬色可自訂RGB背光 \n" +
                        "◆ 有線/無線自由選擇");//set default describe
                break;
            case "鍵盤":
                ItemImage.setImageResource(R.drawable.a10);
                ItemName.setText("Ducky shine5");//set default name
                price=5000/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("採用 Cherry RGB MX 機械軸 \n" +
                        "經典輕薄時尚的外觀 \n" +
                        "雙層PCB電路板 \n" +
                        "LED 背光技術 \n" +
                        "ARM M3 MCU處理器 \n" +
                        "USB1000Hz 回報速率 \n" +
                        "USB輸出字元加速功能 \n" +
                        "USB 介面全鍵輸出 N-Key Rollover \n" +
                        "原廠一年保固");//set default describe
                break;
            case "螢幕":
                ItemImage.setImageResource(R.drawable.a11);
                ItemName.setText("華碩VX238H");//set default name
                price=4000/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("A+防眩光面板 \n" +
                        "不閃屏.低藍光 \n" +
                        "支援D-SUB/DVI/HDMIx2介面 \n" +
                        "1MS反時間 \n" +
                        "Quickfit 虛擬尺標 \n" +
                        "LED背光螢幕，環保節能 \n" +
                        "三年保固 \n");//set default describe
                break;
            case "耳機":
                ItemImage.setImageResource(R.drawable.a12);
                ItemName.setText("羅技G430");//set default name
                price=2490/rate;
                ItemPrice.setText(Double.toString(price));//set default price
                ItemDescribe.setText("◆方便的線控裝置 \n" +
                        "◆7.1 數位音訊 \n" +
                        "◆平折式耳罩可 90 度旋轉");//set default describe
                break;

        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    @OnClick(R.id.imageView2)
    public void backtohome() {
        onBackPressed();
        //tv4.setText("test");
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowItem Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://fintech.practice.csie.ntnu.edu.tw.fintechapp/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ShowItem Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://fintech.practice.csie.ntnu.edu.tw.fintechapp/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
