package fintech.practice.csie.ntnu.edu.tw.fintechapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.appacitive.android.AppacitiveContext;
//import com.appacitive.core.AppacitiveObject;
//import com.appacitive.core.model.AppacitiveStatus;
//import com.appacitive.core.model.Callback;
//import com.appacitive.core.model.Environment;
import com.coinbase.android.sdk.OAuth;
import com.coinbase.api2.Coinbase;
import com.coinbase.api2.entity.OAuthTokensResponse;
import com.coinbase.api2.exception.UnauthorizedException;


import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.button)
    Button btnShowAddress;
    @BindView(R.id.editText)
    EditText editText;

    Handler h;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(MainActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
            }
        }, 1000L);
        //AppacitiveContext.initialize("2Wj+Ue6FTBO+UUrdW4020QQUMtDB0FvDu/AOaNOU8K0=", Environment.sandbox,this);

        //coinbase sdk usage document link: https://github.com/coinbase/coinbase-java
        //coinbase with testnet account sign-up: https://sandbox.coinbase.com/signup
        //coinbase sandbox document: https://developers.coinbase.com/docs/wallet/testing
    }

    public class CompleteAuthorizationTask extends AsyncTask<Void, Void, OAuthTokensResponse> {
        private Intent mIntent;

        public CompleteAuthorizationTask(Intent intent) {
            Log.d("fintech", "initial complete auth task");
            mIntent = intent;
        }

        @Override
        protected OAuthTokensResponse doInBackground(Void... params) {
            Log.d("fintech", "call complete authorization");
            try {
                return OAuth.completeAuthorization(MainActivity.this, Config.clientID, Config.clientSecret, mIntent.getData());
            } catch (UnauthorizedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(OAuthTokensResponse r) {
            if (r == null) {
                Toast.makeText(MainActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                ((FintechApplication) getApplication()).setOAuthResponse(r);
                Log.d("fintech", "token=" + r.getAccessToken());
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("fintech", "got new intent");

        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.VIEW")) {
            Log.d("fintech", "ready to execute CompleteAuthorizationTask");
            new CompleteAuthorizationTask(intent).execute();
        }
    }

    @OnClick(R.id.button)
    public void login() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                //Coinbase cb = ((FintechApplication) getApplication()).getCoinbase();
                try {
                    //new CoinbaseBuilder().withBaseApiURL(new URL("https://api.sandbox.coinbase.com/")).withBaseOAuthURL(new URL("https://sandbox.coinbase.com/")).build();
                    OAuth.beginAuthorization(MainActivity.this, Config.clientID, "user", "fintech://coinbase-oauth", null);
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                    //Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
            }
        }.execute();
    }

    @OnClick(R.id.button2)
    public void showInfo() {
        new AsyncTask<Void, Void, Wallet>() {
            @Override
            protected Wallet doInBackground(Void... params) {
                Coinbase cb = ((FintechApplication) getApplication()).getCoinbase();
                Wallet w = new Wallet();
                try {
                    w.setAccount(cb.getUser().getEmail());
                    w.setAddress("address size = " + cb.getAddresses().getAddresses().size());
                    w.setBalance(cb.getBalance().toString());

                    Log.d("fintech", "address=" + w.getAddress());
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                    //Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
                return w;
            }

            @Override
            protected void onPostExecute(Wallet w) {
                tvAccount.setText(w.getAccount());
                tvAddress.setText(w.getAddress());
                tvBalance.setText(w.getBalance());
            }
        }.execute();
    }


    @OnClick(R.id.button3)
    public void ToItemList()
    {
        //Toast.makeText(MainActivity.this, "item list", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(MainActivity.this, ItemList.class));
    }

    @OnClick(R.id.button4)
    public void loginplatform()
    {
        Toast.makeText(MainActivity.this, editText.getText().toString()+" 登入成功", Toast.LENGTH_SHORT).show();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(MainActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
            }
        }, 2000L);

        Intent intent = new Intent(MainActivity.this, ItemList.class);
        intent.putExtra("email", editText.getText().toString());
        startActivity(intent);
        //startActivity(new Intent(MainActivity.this, ItemList.class));
    }

    @OnClick(R.id.button19)
    public void signup()
    {
        Toast.makeText(MainActivity.this, editText.getText().toString()+" 註冊成功", Toast.LENGTH_SHORT).show();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(MainActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
            }
        }, 2000L);
        //AppacitiveObject user = new AppacitiveObject("account");
        //user.setStringProperty("email", editText.getText().toString());//editText.toString()


       //user.createInBackground(new Callback<AppacitiveObject>() {
           /*@Override
           public void success(AppacitiveObject result) {
               //super.success(result);
               //Intent intent = new Intent(MainActivity.this, ItemList.class);
               //intent.putExtra("email", result);
              // startActivity(intent);
               startActivity(new Intent(MainActivity.this, ItemList.class));
           }

           @Override
           public void failure(AppacitiveObject result, Exception e)
           {
               Toast.makeText(MainActivity.this, "Sign up fail", Toast.LENGTH_SHORT).show();
           }
       });*/

        Intent intent = new Intent(MainActivity.this, ItemList.class);
        intent.putExtra("email", editText.getText().toString());
        startActivity(intent);
    }

}
