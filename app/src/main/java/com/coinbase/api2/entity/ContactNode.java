package com.coinbase.api2.entity;

import java.io.Serializable;

public class ContactNode implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 642700372938561693L;
    private com.coinbase.api2.entity.Contact _contact;

    public com.coinbase.api2.entity.Contact getContact() {
        return _contact;
    }

    public void setContact(com.coinbase.api2.entity.Contact contact) {
        _contact = contact;
    }
}
