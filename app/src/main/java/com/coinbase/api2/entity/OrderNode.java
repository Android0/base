package com.coinbase.api2.entity;

import java.io.Serializable;

public class OrderNode implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 9143405822047138714L;
    private com.coinbase.api2.entity.Order _order;

    public com.coinbase.api2.entity.Order getOrder() {
        return _order;
    }

    public void setOrder(com.coinbase.api2.entity.Order order) {
        _order = order;
    }
}
