package com.coinbase.api2.entity;

public class UserResponse extends com.coinbase.api2.entity.Response {
    /**
     *
     */
    private static final long serialVersionUID = 8847695815066590925L;
    private com.coinbase.api2.entity.User _user;
    private OAuthTokensResponse _oauth;

    public com.coinbase.api2.entity.User getUser() {
        return _user;
    }

    public void setUser(User user) {
        _user = user;
    }

    public OAuthTokensResponse getOauth() {
        return _oauth;
    }

    public void setOauth(OAuthTokensResponse oauth) {
        this._oauth = oauth;
    }
}
