package com.coinbase.api2.entity;

import com.coinbase.api2.entity.RecurringPayment;

public class RecurringPaymentResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = -1740844042364630330L;
    private com.coinbase.api2.entity.RecurringPayment _recurringPayment;

    public com.coinbase.api2.entity.RecurringPayment getRecurringPayment() {
        return _recurringPayment;
    }

    public void setRecurringPayment(RecurringPayment recurringPayment) {
        _recurringPayment = recurringPayment;
    }
}
