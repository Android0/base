package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.deserializer.AddressesLifter;
import com.coinbase.api2.entity.Address;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class AddressesResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 3300893137342086524L;
    private List<com.coinbase.api2.entity.Address> _addresses;
    
    public List<com.coinbase.api2.entity.Address> getAddresses() {
	return _addresses;
    }

    @JsonDeserialize(converter=AddressesLifter.class)
    public void setAddresses(List<Address> addresses) {
	_addresses = addresses;
    }
}
