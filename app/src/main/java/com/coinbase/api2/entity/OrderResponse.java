package com.coinbase.api2.entity;

import com.coinbase.api2.entity.Order;

public class OrderResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 1217894460265345504L;
    private com.coinbase.api2.entity.Order _order;
    
    public com.coinbase.api2.entity.Order getOrder() {
        return _order;
    }

    public void setOrder(Order order) {
        _order = order;
    }
}
