package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.entity.Contact;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class ContactsResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 1579609741624298006L;
    private List<com.coinbase.api2.entity.Contact> _contacts;
    
    public List<com.coinbase.api2.entity.Contact> getContacts() {
	return _contacts;
    }

    @JsonDeserialize(converter= com.coinbase.api2.deserializer.ContactsLifter.class)
    public void setContacts(List<Contact> contacts) {
	_contacts = contacts;
    }
}
