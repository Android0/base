package com.coinbase.api2.entity;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class ReportsResponse extends Response {
    /**
     * 
     */
    private static final long serialVersionUID = -72799672257529179L;
    private List<Report> _reports;
    
    public List<Report> getReports() {
	return _reports;
    }

    @JsonDeserialize(converter= com.coinbase.api2.deserializer.ReportsLifter.class)
    public void setReports(List<Report> reports) {
	_reports = reports;
    }
}
