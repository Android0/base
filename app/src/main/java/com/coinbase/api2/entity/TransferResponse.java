package com.coinbase.api2.entity;

public class TransferResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 5487491088482969188L;
    private Transfer _transfer;
    
    public Transfer getTransfer() {
        return _transfer;
    }

    public void setTransfer(Transfer transfer) {
        _transfer = transfer;
    }
}
