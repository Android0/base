package com.coinbase.api2.entity;

import java.io.Serializable;

public class RecurringPaymentNode implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6607883611045097912L;
    private com.coinbase.api2.entity.RecurringPayment _recurringPayment;

    public com.coinbase.api2.entity.RecurringPayment getRecurringPayment() {
	return _recurringPayment;
    }

    public void setRecurringPayment(com.coinbase.api2.entity.RecurringPayment recurringPayment) {
	_recurringPayment = recurringPayment;
    }
}
