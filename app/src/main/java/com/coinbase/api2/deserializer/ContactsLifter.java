package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.coinbase.api2.entity.Contact;
import com.coinbase.api2.entity.ContactNode;
import com.fasterxml.jackson.databind.util.StdConverter;

public class ContactsLifter extends StdConverter<List<ContactNode>, List<Contact>> {

    public List<Contact> convert(List<ContactNode> nodes) {
	ArrayList<Contact> result = new ArrayList<Contact>();
	
	for (ContactNode node : nodes) {
	    result.add(node.getContact());
	}
	
	return result;
    }

}
