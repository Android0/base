package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.coinbase.api2.entity.Order;
import com.fasterxml.jackson.databind.util.StdConverter;

public class OrdersLifter extends StdConverter<List<com.coinbase.api2.entity.OrderNode>, List<Order>> {

    public List<Order> convert(List<com.coinbase.api2.entity.OrderNode> nodes) {
	ArrayList<Order> result = new ArrayList<Order>();
	
	for (com.coinbase.api2.entity.OrderNode node : nodes) {
	    result.add(node.getOrder());
	}
	
	return result;
    }

}
