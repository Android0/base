package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.coinbase.api2.entity.TransferNode;
import com.fasterxml.jackson.databind.util.StdConverter;

public class TransfersLifter extends StdConverter<List<com.coinbase.api2.entity.TransferNode>, List<com.coinbase.api2.entity.Transfer>> {

    public List<com.coinbase.api2.entity.Transfer> convert(List<TransferNode> nodes) {
	ArrayList<com.coinbase.api2.entity.Transfer> result = new ArrayList<com.coinbase.api2.entity.Transfer>();
	
	for (com.coinbase.api2.entity.TransferNode node : nodes) {
	    result.add(node.getTransfer());
	}
	
	return result;
    }

}
